Проект позволяет распространять модификации (в архиве) и частично реализует функционал соц сети

Сейчас сайт доступен на http://denx24x.pythonanywhere.com/, но
  - Он(pythonanywhere) работает очень медленно
  - Не работают все события (pythonanywhere) не поддерживает создание потоков
  - Не работает взаимодействие с почтой (pythonanywhere) не поддерживает smtp
Так же доступен на http://yandex-web-belozorov.herokuapp.com/, но
  - Heroku не позволяет просто брать и сохранять файлы
Пока что рекомедуется запускать локально

При запуске в базу данных добавляются основные пользователи
admin@this с паролем 123 – пользователь, имеющий все права
test@this с паролем 123 – обычный пользователь
PYTHONIOENCODING=utf-8 python3.6 main.py 2>&1 | tee log &